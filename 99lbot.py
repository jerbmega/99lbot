import json
import re
import time
import discord


global config
client = discord.Client()

def loadConfig():
    # t0 = int(round(time.time() * 1000))
    with open('config.json') as cfg:
        config = json.load(cfg)
    print('config.json loaded')
    return config

config = loadConfig()

@client.event
async def on_message(message):

    global config

    # Logging
    with open('log.txt', 'a') as log:
        log.write(message.id + ' | ' + message.author.name + ' (' + message.author.id + ') | ' + message.content + '\n')

    # Config reloading
    if message.author.id == config['ownerID'] and message.content == config['prefix'] + '99l_reloadcfg':
        await client.delete_message(message)
        config = loadConfig()

    # Deletion check - override for staff, apply for all others
    if message.author == client.user or str(message.author.top_role) == 'Mods' or str(message.author.top_role) == 'Artists' or str(message.author.top_role) == 'Ninety9Lives':
        return
    else:
        for deletion in config['deletions']:
            if deletion['trigger'] in re.sub(r'\s+', '', message.content.lower()):
                await client.delete_message(message)
                await client.send_message(message.author, deletion['response'] + config['footer'])
                logmsg = '**Blacklisted phrase:** <@{id}> {log} in <#{channel_id}>'.format(id=message.author.id, channel_id=message.channel.id, log=deletion['log'])
                embed = discord.Embed(color=0x730303, title=message.content)
                await client.send_message(client.get_channel(config['report_id']), logmsg, embed=embed)


@client.event
async def on_message_edit(oldmsg, newmsg):
    with open('log.txt', 'a') as log:
        log.write(newmsg.id + ' (edited) | ' + newmsg.author.name + ' (' + newmsg.author.id + ') | ' + newmsg.content + '\n')

    if newmsg.author == client.user or str(newmsg.author.top_role) == 'Mods' or str(newmsg.author.top_role) == 'Artists' or str(newmsg.author.top_role) == 'Ninety9Lives':
        return
    else:
        for deletion in config['deletions']:
            if deletion['trigger'] in re.sub(r'\s+', '', newmsg.content.lower()):
                await client.delete_message(newmsg)
                await client.send_message(newmsg.author, deletion['response'] + config['footer'])
                logmsg = '**Blacklisted phrase:** <@{id}> {log} in <#{channel_id}> \nAdditionally, a **filter bypass attempt** was detected (user edited blacklisted phrase into an old message).'.format(id=newmsg.author.id, channel_id=newmsg.channel.id, log=deletion['log'])
                embed = discord.Embed(color=0x730303, title=newmsg.content)
                await client.send_message(client.get_channel(config['report_id']), logmsg, embed=embed)


@client.event
async def on_member_join(member):
    await client.send_message(member, 'Welcome to the Ninety9Lives server, ' + member.mention + '!' + config['join_message'])
    embed = discord.Embed(color=0x398ED6, title="A user has joined.")
    embed.add_field(name="-----", value="User {name}#{discrim} with ID {id} has joined the server.".format(name=member.name, discrim=member.discriminator, id=member.id))
    embed.set_thumbnail(url=member.avatar_url)
    await client.send_message(client.get_channel(config['report_id']), embed=embed)

@client.event
async def on_member_remove(member):
    embed = discord.Embed(color=0xD63939, title="A user has left.")
    embed.add_field(name="-----", value="User {name}#{discrim} with ID {id} has left the server. They had the nickname of {nick}.".format(name=member.name, discrim=member.discriminator, id=member.id, nick=member.nick))
    embed.set_thumbnail(url=member.avatar_url)
    #embed.set_author(name=member.name + "#" + member.discriminator + " left", icon_url=member.avatar_url)
    await client.send_message(client.get_channel(config['report_id']), embed=embed)


@client.event
async def on_ready():
    print('99lbot.py')
    print('Logged in as ' + client.user.name)
    print('----------')

client.run(config['token'])
